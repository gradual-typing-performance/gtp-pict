#lang scribble/manual

@require[
  scribble/example
  (for-label
    gtp-pict
    pict
    ppict/2
    pict-abbrevs
    images/icons/style
    (only-in racket/math natural?)
    racket/base
    racket/class
    racket/contract
    racket/draw)]

@(define gtp-pict-eval (make-base-eval '(require pict gtp-pict)))

@; -----------------------------------------------------------------------------
@title[#:tag "top"]{GTP pict}

@defmodule[gtp-pict]{
  Miscellaneous gradual typing picts.
}

@section{GTP pict functions}

@defproc[(make-racket-logo) pict?]{
  Make a pict with the Racket logo.

@;  @examples[#:eval gtp-pict-eval
@;    (frame (scale (make-racket-logo) 100 100))]
}

@defproc[(tau-icon [#:color color pict-color/c (*tau-color*)]
                   [#:height height real? (default-icon-height)]
                   [#:material material icon-material? (default-icon-material)]) (is-a?/c bitmap%)]{
  @examples[#:eval gtp-pict-eval
    (bitmap (tau-icon))]
}

@defproc[(icon-material? [x any/c]) boolean?]{
  Predicate for icon materials.
  Similar to @racket[(or/c plastic-icon-material rubber-icon-material glass-icon-material metal-icon-material)].
  See also @racketmodname[images/icons/style].
}

@deftogether[(
  @defproc[(make-program-pict/sta) pict?]
  @defproc[(make-program-pict/dyn) pict?]
  @defproc[(make-program-pict/mixed [#:show-boundary? show-boundary? #false]) pict?]
  @defproc[(make-component-pict/sta [#:body body (or/c #f pict?) #f]
                                    [#:width width (or/c #f real?) #f]
                                    [#:height height (or/c #f real?) #f]) pict?]
  @defproc[(make-component-pict/dyn [#:body body (or/c #f pict?) #f]
                                    [#:width width (or/c #f real?) #f]
                                    [#:height height (or/c #f real?) #f]) pict?]
  @defproc[(make-component-pict/blank [#:body body (or/c #f pict?) #f]
                                      [#:width width (or/c #f real?) #f]
                                      [#:height height (or/c #f real?) #f]) pict?]
  @defproc[(make-component-pict/tu [#:body body pict?]
                                   [#:width width real?]
                                   [#:height height real?]
                                   [#:color color pict-color/c]) pict?]
  @defproc[(make-component-pict [#:body body pict? (blank)]
                                [#:width width (or/c #f real?) #f]
                                [#:height height (or/c #f real?) #f]
                                [#:color color (or/c #f real?) #f]
                                [#:border-width border-width real? 4]
                                [#:border-color border-color pict-color/c #f]
                                [#:radius radius real? -0.08]
                                [#:x-margin x-margin real? 10]
                                [#:y-margin y-margin real? 10]) pict?]

)]{
  @examples[#:eval gtp-pict-eval
   (make-program-pict/sta)
   (make-program-pict/dyn)
   (make-program-pict/mixed #:show-boundary? #true)]
} 

@defproc[(make-boundary-pict [#:l l-pict (or/c pict? #f)]
                             [#:c c-pict (or/c pict? #f)]
                             [#:r r-pict (or/c pict? #f)]
                             [#:x-margin x-margin real?]) pict?]{
  Illustrate a value crossing a type boundary.

  @examples[#:eval gtp-pict-eval
    (make-boundary-pict #:l (text "\"hello\"" (cons (*stat-color*) '()) 20)
                        #:c (text "v" '() 20)
                        #:r (text "Int" '() 20))]
}

@defproc[(bool*->bitstring [b* (listof boolean?)]) string?]{
  @examples[#:eval gtp-pict-eval
   (bool*->bitstring '(#t #t))
   (bool*->bitstring '(#f #t #f #f))]
}

@defproc[(bool*->tag [b* (listof boolean?)]) symbol?]{
  @examples[#:eval gtp-pict-eval
    (bool*->tag '(#t #f #t #t))]
}

@defproc[(bitstring->tag [s string?]) symbol?]{
  @examples[#:eval gtp-pict-eval
    (bitstring->tag "101")]
}

@defproc[(make-node [b* (listof boolean?)]) pict?]{
  @examples[#:eval gtp-pict-eval
    (scale (make-node '(#t #t #t #f #t)) 1/2)]
}

@defproc[(make-lattice [total-bits natural?]
                       [make-node (-> (listof boolean?) pict?)]
                       [#:x-margin x-margin (*lattice-node-x-margin*)]
                       [#:y-margin y-margin (*lattice-node-y-margin*)]) pict?]{
  @examples[#:eval gtp-pict-eval
    (scale-to-fit (make-lattice 3 make-node) 200 600)]
}

@defproc[(make-fraction [top pict?] [bot pict?]) pict?]{
  @examples[#:eval gtp-pict-eval
    (make-fraction (standard-fish 50 30) (jack-o-lantern 40))]
}

@defproc[(make-check-x-fraction) pict?]{
  @examples[#:eval gtp-pict-eval
    (make-check-x-fraction)]
}

@deftogether[(
  @defproc[(make-icon [i (-> #:material icon-material? #:height real? (is-a?/c bitmap%))] [#:height h real?]) pict?]{}
  @defproc[(small-check-icon) pict?]
  @defproc[(small-x-icon) pict?]
  @defproc[(small-tau-icon) pict?]
  @defproc[(small-lambda-icon) pict?]
)]{
  @examples[#:eval gtp-pict-eval
    (small-check-icon)
    (small-x-icon)
    (small-tau-icon)
    (small-lambda-icon)]
}

@defproc[(make-2table [picts (listof (cons/c pict? pict?))]
                      [#:col-sep col-sep (or/c real? #f) 328/5]
                      [#:row-sep row-sep (or/c real? #f) 364/5]
                      [#:col-align col-align lc-superimpose]
                      [#:row-align row-align cc-superimpose]) pict?]{
  Create a @racket[table] with 2 columns.

  @examples[#:eval gtp-pict-eval
    (make-2table (list (cons (standard-fish 40 40) (small-check-icon))
                       (cons (jack-o-lantern 40) (small-x-icon)))
                 #:row-sep 30)]
}


@section{Deep, Shallow, Erasure}

``Deep'' ``Shallow'' and ``Erasure'' are three possible names for three common
approaches to enforcing types at runtime in a mixed-typed (or, gradually typed)
language.

@deftogether[(
  @defthing[#:kind "value" DEEP-TAG symbol? #:value 'deep]
  @defthing[#:kind "value" SHALLOW-TAG symbol? #:value 'shallow]
  @defthing[#:kind "value" ERASURE-TAG symbol? #:value 'erasure]
  @defthing[#:kind "value" DSE-TAG* (listof symbol?) #:value (list DEEP-TAG SHALLOW-TAG ERASURE-TAG)]
  @defproc[(dse-tag? [x any/c]) boolean?]{}
  @defproc[(dse->string [x dse-tag?]) string?]
  @defproc[(dse->letter [x dse-tag?]) string?]
  @defproc[(dse->apples-name [x dse-tag?]) string?]
  @defproc[(dse->kafka-name [x dse-tag?]) string?]
  @defproc[(dse->color% [x dse-tag?]) (is-a?/c color%)]
)]


@section{GTP pict parameters}

@deftogether[(
  @defparam[*component-color* cc (is-a?/c color%) #:value (string->color% "LightGoldenrodYellow")]{}
  @defparam[*component-border-color* cbc (is-a?/c color%) #:value (string->color% "black")]{}
  @defparam[*dyn-color* dc (is-a?/c color%) #:value (string->color% "Firebrick")]{}
  @defparam[*dyn-text-color* dtc (is-a?/c color%) #:value (string->color% light-metal-icon-color)]{}
  @defparam[*stat-color* sc (is-a?/c color%) #:value (string->color% "PaleTurquoise")]{}
  @defparam[*lambda-color* lc (is-a?/c color%) #:value light-metal-icon-color]{}
  @defparam[*tau-color* tc (is-a?/c color%) #:value (string->color% "DarkGoldenrod")]{}
  @defparam[*deep-color* dc (is-a?/c color%) #:value (string->color% "Tomato")]{}
  @defparam[*shallow-color* sc (is-a?/c color%) #:value (string->color% "cornflowerblue")]{}
  @defparam[*erasure-color* ec (is-a?/c color%) #:value (string->color% "mediumseagreen")]{}
  @defparam[*lattice-node-color* lnc (is-a?/c color%) #:value (string->color% "Gainsboro")]{}
  @defparam[*boundary-color* bc (is-a?/c color%) #:value (string->color% "black")]{}
  @defparam[*type-boundary-color* tbc (is-a?/c color%) #:value (string->color% "Fuchsia")]{}
  @defparam[*line-margin* lm real? #:value 4]{}
  @defparam[*lattice-component-margin* lcm real? #:value 6]{}
  @defparam[*lattice-node-x-margin* lnxm real? #:value 20]
  @defparam[*lattice-node-y-margin* lnym real? #:value 40]
  @defparam[*component-x-margin* cxm real? #:value 40]
  @defparam[*component-arrow-size* cas real? #:value 11]
  @defparam[*component-arrow-width* caw real? #:value 3]
  @defparam[*type-boundary-arrow-size* tbas real? #:value 13]
  @defparam[*type-boundary-arrow-width* tbaw real? #:value 4]
  @defparam[*migration-arrow-size* mas real? #:value 25]
  @defparam[*migration-arrow-width* maw real? #:value 8]
  @defparam[*tau-font* tf (is-a?/c font%)]
)]

@section{GTP pict constants}

@deftogether[(
  @defthing[#:kind "value" TYPE-BOUNDARY-TAG symbol? #:value 'type-boundary]
  @defthing[#:kind "value" SLIDE-TOP real% #:value 1/10]
  @defthing[#:kind "value" SLIDE-LEFT real% #:value 1/50]
  @defthing[#:kind "value" SLIDE-BOTTOM real% #:value 4/5]
  @defthing[#:kind "value" SLIDE-RIGHT real% #:value 49/50]
  @defthing[#:kind "value" HEADING-COORD refpoint-placer?]
  @defthing[#:kind "value" -HEADING-COORD refpoint-placer?]
  @defthing[#:kind "value" CENTER-COORD refpoint-placer?]
)]

